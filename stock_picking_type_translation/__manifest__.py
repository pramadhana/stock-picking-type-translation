# -*- coding: utf-8 -*-
{
    'name': "Stock Picking Type Translation",

    'summary': """
        Stock Picking Type Translation Module""",

    'description': """
        Translation for stock picking type in Indonesian (Bahasa Indonesia)
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock'],

    # always loaded
    'data': [
        'views/views.xml', # execute funtion to translate stock picking type
    ],
}
