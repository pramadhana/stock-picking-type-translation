# -*- coding: utf-8 -*-

from odoo import models, fields, api

import logging
_logger = logging.getLogger(__name__)

class IrTranslation(models.Model):
    _inherit = "ir.translation"

    # function to create translation for new stock picking type record
    # the stock picking type that will be translated:
    #    1. Receipts
    #    2. Delivery Orders
    @api.model
    def translate_stock_picking_type(self):
        stock_picking_type_ids = self.env['stock.picking.type'].search([]) # getting all stock picking types
        for item in stock_picking_type_ids:
            if item.name == 'Receipts':
                if len(self.search([('name','=','stock.picking.type,name'),('src','=',item.name),('res_id','=',item.id)])) == 0: # check if there is no translation
                    values_id = {
                        'name': 'stock.picking.type,name',
                        'res_id': item.id,
                        'lang': 'id_ID',
                        'type': 'model',
                        'src': item.name,
                        'value': 'Penerimaan',
                        'state': 'translated',
                    }
                    values_en = {
                        'name': 'stock.picking.type,name',
                        'res_id': item.id,
                        'lang': 'en_US',
                        'type': 'model',
                        'src': item.name,
                        'value': item.name,
                        'state': 'translated',
                    }
                    _logger.info("Creating new translation for %s" % item.name)
                    self.create(values_id) # translation for id_ID
                    self.create(values_en) # translation for en_US

            elif item.name == 'Delivery Orders':
                if len(self.search([('name','=','stock.picking.type,name'),('src','=',item.name),('res_id','=',item.id)])) == 0: # check if there is no translation
                    values_id = {
                        'name': 'stock.picking.type,name',
                        'res_id': item.id,
                        'lang': 'id_ID',
                        'type': 'model',
                        'src': item.name,
                        'value': 'Order Pengiriman',
                        'state': 'translated',
                    }
                    values_en = {
                        'name': 'stock.picking.type,name',
                        'res_id': item.id,
                        'lang': 'en_US',
                        'type': 'model',
                        'src': item.name,
                        'value': item.name,
                        'state': 'translated',
                    }
                    _logger.info("Creating new translation for %s" % item.name)
                    self.create(values_id) # translation for id_ID
                    self.create(values_en) # translation for en_US